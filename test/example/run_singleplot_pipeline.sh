#!/bin/bash

WD=/content
PLOTS_DIR=$WD/test/example/plots

###run the build script to generate the key files from all the conditions
python3 $WD/scripts/build_keys.py $PLOTS_DIR --cores=2

SCENARIO=varWC_rx1_cond31566_site2_climEnsemble-rcp45
KEYWORD_DIR=$PLOTS_DIR/$SCENARIO
FILENAME=$SCENARIO\_off0.key
KEYWORDFILE=$KEYWORD_DIR/$FILENAME
FVS_VARIANT=FVSwc
###run FVS batched on all the different key files
$FVS_VARIANT --keywordfile=$KEYWORDFILE
