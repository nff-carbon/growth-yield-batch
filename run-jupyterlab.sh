#!/bin/bash
PORT=8080
docker run -it --rm \
	-p $PORT:$PORT \
	-v $(pwd):/content \
	-v /datadrive:/data \
       	-w /content \
	cspinc/fvs:growth-yield-batch \
	jupyter lab \
		--ip=0.0.0.0 \
		--port=$PORT \
		--no-browser \
		--allow-root
