#!/bin/bash
PORT=8787
docker run -it --rm\
	--name growth-yield-batch2 \
       -p $PORT:$PORT\
       -v $(pwd):/content\
       -v /datadrive:/data \
       -w /content cspinc/fvs:growth-yield-batch\
       /bin/bash
